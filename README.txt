# sim openocd test

in the soc directory, create the verilog file

    "python issuer_verilog.py libresoc.v"

copy to libresoc/ directory and open a second terminal

terminal 1:

    ./sim.py

terminal 2:

    openocd -f openocd.cfg -c init -c 'svf idcode_test2.svf'

# ecp5 build

same thing: first build libresoc.v and copy it to the libresoc/ directory

    ./versa_ecp5.py --sys-clk-freq=55e6 --build --yosys-nowidelut
    ./versa_ecp5.py --sys-clk-freq=55e6 --load

ulx3s:

    ./versa_ecp5.py --sys-clk-freq=12.5e6 --build --fpga=ulx3s85f \
                    --yosys-nowidelut
    ./versa_ecp5.py --sys-clk-freq=12.5e6 --load --fpga=ulx3s85f

# arty a7 build

    export PATH=$PATH:/usr/local/symbiflow/bin/:/usr/local/symbiflow/vtr/bin/
    ./versa_ecp5.py --sys-clk-freq=25e6 --build  --fpga=artya7100t \
                        --toolchain=symbiflow
    ./versa_ecp5.py --sys-clk-freq=25e6 --load --fpga=artya7100t  \
                        --toolchain=symbiflow

